TEX_PATH := ./tex
ASSETS_PATH := ./assets
TEX_FILES := $(wildcard $(TEX_PATH)/*.tex)
PDF_FILES := $(wildcard $(ASSETS_PATH)/*.pdf)

BUILD_DIR := ./build
PDFLATEX_FLAGS := -halt-on-error # -output-directory $(BUILD_DIR)

OUTPUT_PDF := funcan-2018.pdf

all: $(OUTPUT_PDF)

$(OUTPUT_PDF): $(TEX_FILES)
	cp $(PDF_FILES) ./
	pdftk *.pdf cat output $@

$(TEX_PATH)/%.tex:
	pdflatex $(PDFLATEX_FLAGS) $@
	# the second pass for \ref
	pdflatex $(PDFLATEX_FLAGS) $@

build_dir:
	mkdir -p $(BUILD_DIR)

clean:
	rm *.pdf *.log *.aux *.thm